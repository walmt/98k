package util;

/*
 * help to calculate the distance between two position
 */
public class DistanceUtil {
    private static final double EARTH_RADIUS = 6378.137;

    public static Integer calculateDistance(Double longitude1, Double latitude1, Double longitude2, Double latitude2) {
        double a, b, d, sa2, sb2;
        latitude1 = rad(latitude1);
        latitude2 = rad(latitude2);
        a = latitude1 - latitude2;
        b = rad(longitude1 - longitude2);

        sa2 = Math.sin(a / 2.0);
        sb2 = Math.sin(b / 2.0);
        d = 2   * EARTH_RADIUS
                * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(latitude1)
                * Math.cos(latitude2) * sb2 * sb2));
        return (int)(d * 1000);
    }

    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }
}
