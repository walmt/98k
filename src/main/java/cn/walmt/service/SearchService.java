package cn.walmt.service;

import cn.walmt.dto.LabelAndTimes;
import cn.walmt.dto.PostWithUser;
import cn.walmt.mapper.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SearchService {

    @Autowired
    private PostMapper postMapper;

    public List<LabelAndTimes> getHotTags() {
        return postMapper.selectHotTags();
    }

    public List<PostWithUser> searchByTag(String tag) {
        return postMapper.selectPostsByTag(tag);
    }
}
