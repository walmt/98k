package cn.walmt.service;


import cn.walmt.dto.PostWithUser;
import cn.walmt.entity.Post;
import cn.walmt.entity.User;
import cn.walmt.mapper.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import util.DistanceUtil;
import util.GsonUtil;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class PostService {

    @Autowired
    private PostMapper postMapper;

    /*
     * default page size = 10
     */
    private final static Integer PAGE_SIZE = 10;

    private final static Long DISTANCE = 1000L;

    public PostWithUser getById(Integer id) {
        return postMapper.selectWithUserInfoById(id);
    }

    public List<PostWithUser> getFriendPosts(Integer id, Integer pageId) {
        List<PostWithUser> posts = postMapper.selectFriendPosts(id, (pageId - 1) * PAGE_SIZE, PAGE_SIZE);
        return posts;
    }

    public List<PostWithUser> getPostsAround(Double longitude, Double latitude, Integer pageId) {
        List<PostWithUser> posts = postMapper.selectPostsAround(longitude, latitude, DISTANCE, (pageId - 1) * PAGE_SIZE, PAGE_SIZE);
        for(PostWithUser post: posts) {
            post.setDistance(DistanceUtil.calculateDistance(longitude, latitude, post.getLongitude(), post.getLatitude()));
        }
        return posts;
    }

    public List<Post> getPostsByUserId(Integer id) {
        List<Post> posts = postMapper.selectByUserId(id);
        return posts;
    }

    public void addPost(Post post) {
        postMapper.insert(post);
    }

    public List<PostWithUser> getPostsByStore(String store) {
        return postMapper.selectPostsByStore(store);
    }

    public String getImgLabel(MultipartFile img, HttpSession session) {
        try {
            User user = (User) session.getAttribute("user");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String imgPath = session.getServletContext().getRealPath("/WEB-INF/") + "tmp/" + user.getId() + "-" + sdf.format(new Date());
            img.transferTo(new File(imgPath));
            System.out.println(imgPath);
            String cmd = "python /tencent.py " + imgPath;
            Process process = Runtime.getRuntime().exec(cmd);
            InputStreamReader ir = new InputStreamReader(
                    process.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = input.readLine()) != null) {
                sb.append(line);
            }
            return GsonUtil.getSuccessJson(sb.toString());
        } catch (java.io.IOException e) {
            System.err.println("IOException " + e.getMessage());
            return GsonUtil.getErrorJson("未知错误");
        }
    }
}