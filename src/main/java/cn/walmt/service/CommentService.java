package cn.walmt.service;

import cn.walmt.bean.Msg;
import cn.walmt.dto.CommentDetail;
import cn.walmt.entity.Comment;
import cn.walmt.entity.Post;
import cn.walmt.entity.Reply;
import cn.walmt.entity.User;
import cn.walmt.mapper.CommentMapper;
import cn.walmt.mapper.PostMapper;
import cn.walmt.mapper.ReplyMapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import util.GsonUtil;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * 评论方面的功能
 * Created by walmt on 2018/5/26.
 */
@Service
@Transactional
public class CommentService {

    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ReplyMapper replyMapper;

    @Autowired
    private PostMapper postMapper;

    /**
     * 用户评论
     *
     * @param postId
     * @param content
     * @param session
     * @return
     */
    public String comment(Integer postId, String content, HttpSession session) {
        if (postId == null) {
            return GsonUtil.getErrorJson("postId不能为空");
        }
        if (content == null || content.equals("")) {
            return GsonUtil.getErrorJson("content不能为空");
        }
        if (content.length() > 255) {
            return GsonUtil.getErrorJson("content内容过长");
        }
        User user = (User) session.getAttribute("user");
        Post post = postMapper.selectByPrimaryKey(postId);
        post.setComments(post.getComments() + 1);
        postMapper.updateByPrimaryKey(post);
        Comment comment = new Comment();
        comment.setUserId(user.getId());
        comment.setPostId(postId);
        comment.setContent(content);
        comment.setTime(new Date());
        commentMapper.insertSelective(comment);
        return GsonUtil.getSuccessJson();
    }

    /**
     * 对评论进行回复
     *
     * @param commentId
     * @param receiverId
     * @param content
     * @param session
     * @return
     */
    public String reply(Integer commentId, Integer receiverId, String content, HttpSession session) {
        if (commentId == null) {
            return GsonUtil.getErrorJson("commentId不能为空");
        }
        if (receiverId == null) {
            return GsonUtil.getErrorJson("receiverId不能为空");
        }
        if (content == null || content.equals("")) {
            return GsonUtil.getErrorJson("content不能为空");
        }
        if (content.length() > 255) {
            return GsonUtil.getErrorJson("content内容过长");
        }


        User user = (User) session.getAttribute("user");
        Reply reply = new Reply();
        reply.setResponderId(user.getId());
        reply.setReceiverId(receiverId);
        reply.setCommentId(commentId);
        reply.setTime(new Date());
        reply.setContent(content);
        replyMapper.insertSelective(reply);
        return GsonUtil.getSuccessJson();
    }

    public String getComments(Integer postId) {
        if (postId == null) {
            return GsonUtil.getErrorJson("postId不能为空");
        }
        List<CommentDetail> commentDetails = commentMapper.getComments(postId);
        Gson gson = GsonUtil.getGsonWithDateTimePattern("yyyy-MM-dd HH:mm:ss");
        return gson.toJson(new Msg("success", commentDetails));
    }
}
