package cn.walmt.controller;

import cn.walmt.exception.HttpException;
import cn.walmt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import util.GsonUtil;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * 用户方面的功能
 * Created by walmt on 2018/1/17.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 通过id获取用户信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectUserById", method = RequestMethod.GET)
    public String selectUserById(Integer id) {
        return userService.selectUserById(id);
    }

    /**
     * 登陆功能
     *
     * @param access_token
     * @param openid
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(String access_token, String openid, HttpSession session) {
        try {
            return userService.login(access_token, openid, session);
        } catch (HttpException e) {
            e.printStackTrace();
            return GsonUtil.getErrorJson("网络出错，请重试！");
        }
    }

}
