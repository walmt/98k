package cn.walmt.controller;

import cn.walmt.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * 评论方面的功能
 * Created by walmt on 2018/5/26.
 */
@Controller
@RequestMapping(value = "/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * 评论
     *
     * @param postId
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String comment(Integer postId, String content, HttpSession session) {
        return commentService.comment(postId, content, session);
    }

    /**
     * 通过动态id获取评论
     * @param postId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getComments", method = RequestMethod.GET)
    public String getComments(Integer postId) {
        return commentService.getComments(postId);
    }

    /**
     * 对评论进行回复
     * @param commentId
     * @param receiverId
     * @param content
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/reply", method = RequestMethod.POST)
    public String reply(Integer commentId, Integer receiverId, String content, HttpSession session) {
        return commentService.reply(commentId, receiverId, content, session);
    }

}
