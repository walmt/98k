package cn.walmt.controller;

import cn.walmt.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * 点赞方面的功能
 * Created by walmt on 2018/5/26.
 */
@Controller
@RequestMapping(value = "/vote")
public class VoteController {

    @Autowired
    private VoteService voteService;

    /**
     * 点赞
     *
     * @param postId 动态的id
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/vote", method = RequestMethod.POST)
    public String vote(Integer postId, HttpSession session) {
        return voteService.vote(postId, session);
    }

    /**
     * 取消点赞
     *
     * @param postId
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/cancelVote", method = RequestMethod.POST)
    public String cancelVote(Integer postId, HttpSession session) {
        return voteService.cancelVote(postId, session);
    }

}
