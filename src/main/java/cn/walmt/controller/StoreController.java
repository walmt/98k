package cn.walmt.controller;

import cn.walmt.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/store")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @ResponseBody
    @RequestMapping(value = "/getStoreByPostId", method = RequestMethod.GET)
    public String getStoreByPostId(Integer postId) {
        return storeService.getStoreByPostId(postId);
    }

    @ResponseBody
    @RequestMapping(value = "/getAllStores", method = RequestMethod.GET)
    public String getAllStores(String keyword) {
        return storeService.getAllStores(keyword);
    }

}
