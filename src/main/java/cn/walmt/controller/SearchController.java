package cn.walmt.controller;


import cn.walmt.dto.LabelAndTimes;
import cn.walmt.dto.PostWithUser;
import cn.walmt.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import util.GsonUtil;

import java.util.List;

/*
 * 搜索相关功能
 * 获取热门标签，根据标签搜索动态等
 */
@Controller
@RequestMapping(value = "/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @ResponseBody
    @RequestMapping(value = "/getHotTags", method = RequestMethod.GET)
    public String getHotTags() {
        List<LabelAndTimes> tags = searchService.getHotTags();
        return GsonUtil.getSuccessJson(tags);
    }

    @ResponseBody
    @RequestMapping(value = "/getPosts", method = RequestMethod.GET)
    public String getPosts(String tag) {
        List<PostWithUser> posts = searchService.searchByTag(tag);
        return GsonUtil.getSuccessJson(posts);
    }

}
