package cn.walmt.entity;

import java.util.Date;

/**
 * 动态表
 */
public class Post {
    private Integer id;

    private Integer userId; // 用户id

    private String content; // 动态

    private String imgUrl; // 图片url

    private String position; // 位置信息

    private Double longitude; // 经度

    private Double latitude; // 纬度

    private String store; // 店名

    private String imgLabel; // 图片标签

    private Date releaseTime; // 发布时间

    private Long cost = 0L; // 花费

    private Integer votes = 0; // 点赞数

    private Integer comments = 0; // 评论数

    private String district; // 地区名

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store == null ? null : store.trim();
    }

    public String getImgLabel() {
        return imgLabel;
    }

    public void setImgLabel(String imgLabel) {
        this.imgLabel = imgLabel == null ? null : imgLabel.trim();
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", userId=" + userId +
                ", content='" + content + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", position='" + position + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", store='" + store + '\'' +
                ", imgLabel='" + imgLabel + '\'' +
                ", releaseTime=" + releaseTime +
                ", cost=" + cost +
                ", votes=" + votes +
                ", comments=" + comments +
                ", district='" + district + '\'' +
                '}';
    }
}