package cn.walmt.entity;

/**
 * 用户表
 */
public class User {

    public static final Boolean SEX_MALE = Boolean.TRUE; // 男
    public static final Boolean SEX_FEMALE = Boolean.FALSE; // 女
    public static final Boolean SEX_UNKNOWN = null; // 未知

    private Integer id;

    private String openid; // QQ对应openid

    private String username; // 用户名

    private String headUrl; // 头像url

    private Boolean sex; // 性别

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl == null ? null : headUrl.trim();
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }
}