package cn.walmt.pojo;

/**
 * Created by walmt on 2018/5/26.
 */
public class PostIdAndUserId {

    private Integer postId;
    private Integer userId;

    public PostIdAndUserId() {
    }

    public PostIdAndUserId(Integer postId, Integer userId) {
        this.postId = postId;
        this.userId = userId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
