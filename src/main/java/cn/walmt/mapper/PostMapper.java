package cn.walmt.mapper;

import cn.walmt.dto.LabelAndTimes;
import cn.walmt.dto.PostWithUser;
import cn.walmt.entity.Post;
import cn.walmt.entity.PostExample;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface PostMapper {
    int countByExample(PostExample example);

    int deleteByExample(PostExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Post record);

    int insertSelective(Post record);

    List<Post> selectByExample(PostExample example);

    Post selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Post record, @Param("example") PostExample example);

    int updateByExample(@Param("record") Post record, @Param("example") PostExample example);

    int updateByPrimaryKeySelective(Post record);

    int updateByPrimaryKey(Post record);

    void incrementVotes(Integer postId);

    void decrementVotes(Integer postId);

    List<LabelAndTimes> selectHotTags();

    List<PostWithUser> selectPostsByTag(@Param("tag") String tag);

    List<PostWithUser> selectFriendPosts(@Param("id") Integer id, @Param("limitBegin") Integer limitBegin, @Param("pageSize") Integer pageSize);

    List<PostWithUser> selectPostsAround(@Param("pos_longitude") Double longitude, @Param("pos_latitude") Double latitude, @Param("distance") Long distance,
                                         @Param("limit_begin") Integer limitBegin, @Param("page_size") Integer pageSize);

    List<Post> selectByUserId(@Param("userId") Integer id);

    PostWithUser selectWithUserInfoById(@Param("id") Integer id);

    List<PostWithUser> selectPostsByStore(@Param("store") String store);

    List<Post> selectAllPosts(String keyword);
}