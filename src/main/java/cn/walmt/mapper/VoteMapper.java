package cn.walmt.mapper;

import cn.walmt.entity.Vote;
import cn.walmt.entity.VoteExample;
import java.util.List;

import cn.walmt.pojo.PostIdAndUserId;
import org.apache.ibatis.annotations.Param;

public interface VoteMapper {
    int countByExample(VoteExample example);

    int deleteByExample(VoteExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Vote record);

    int insertSelective(Vote record);

    List<Vote> selectByExample(VoteExample example);

    Vote selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Vote record, @Param("example") VoteExample example);

    int updateByExample(@Param("record") Vote record, @Param("example") VoteExample example);

    int updateByPrimaryKeySelective(Vote record);

    int updateByPrimaryKey(Vote record);

    Vote selectByPostIdAndUserId(PostIdAndUserId postIdAndUserId);

    Long getCountByPostId(Integer postId);

    Vote isVote(Vote vote);
}