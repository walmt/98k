package cn.walmt.dto;

import java.util.Date;

/**
 * Created by walmt on 2018/5/28.
 */
public class CommentDetail {

    private Integer userId;
    private Integer commentId;
    private String content;
    private Date time;
    private String username;
    private String headUrl;

    public CommentDetail() {
    }

    public CommentDetail(Integer userId, Integer commentId, String content, Date time, String username, String headUrl) {
        this.userId = userId;
        this.commentId = commentId;
        this.content = content;
        this.time = time;
        this.username = username;
        this.headUrl = headUrl;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }
}
