package cn.walmt.dto;

import cn.walmt.entity.Comment;
import cn.walmt.entity.Post;
import cn.walmt.entity.User;

import java.util.Date;
import java.util.List;

/*
 * PostWithUser + comments
 */
public class PostDetail {

    private Integer id;

    private Integer userId; // 用户id

    private String username;  //用户名

    private String headUrl;   //用户头像

    private String content; // 内容

    private String contentUrl; // 图片url

    private String location; // 位置信息

    private Double longitude; // 经度

    private Double latitude; // 纬度

    private String store; // 店名

    private String imgLabel; // 图片标签

    private Date releaseTime; // 发布时间

    private Long cost = 0L; // 花费

    private Integer votes = 0; // 点赞数

    private Integer comments = 0; // 评论数

    private String district; // 地区名

    private String distance;  //距离

    private Boolean isVote; // 是否已点赞

    private List<CommentDetail>  mCommentList;

    public PostDetail(Post post, User user, List<CommentDetail> mCommentList, String distance, boolean isVote) {
        this.id = post.getId();
        this.userId = post.getUserId();
        this.username = user.getUsername();
        this.headUrl = user.getHeadUrl();
        this.content = post.getContent();
        this.contentUrl = post.getImgUrl();
        this.location = post.getPosition();
        this.longitude = post.getLongitude();
        this.latitude = post.getLatitude();
        this.store = post.getStore();
        this.imgLabel = post.getImgLabel();
        this.releaseTime = post.getReleaseTime();
        this.cost = post.getCost();
        this.votes = post.getVotes();
        this.comments = post.getComments();
        this.district = post.getDistrict();
        this.mCommentList = mCommentList;
        this.distance = distance;
        this.isVote = isVote;
    }


}
