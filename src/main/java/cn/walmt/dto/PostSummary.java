package cn.walmt.dto;

import cn.walmt.entity.Post;
import cn.walmt.entity.User;

import java.util.List;

/*
 * 某人的动态
 * 个人信息 + 所有动态 + [总的花费]
 */
public class PostSummary {
    private Integer userId;

    private String username;

    private String headUrl;

    private Long cost = 0L;

    private List<Post> posts;

    public PostSummary(User user, List<Post> posts) {
        this.userId = user.getId();
        this.username = user.getUsername();
        this.headUrl = user.getHeadUrl();
        this.posts = posts;
        for(Post post: posts) {
            cost += post.getCost();
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
