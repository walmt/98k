package cn.walmt.dto;

/**
 * Created by walmt on 2018/6/1.
 */
public class LabelAndTimes {

    private String label;
    private String times;

    public LabelAndTimes() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }
}
