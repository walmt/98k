package cn.walmt.exception;

public class HttpException extends Exception {

    public HttpException() {
    }

    public HttpException(String message) {
        super(message);
    }
}
